/*
Copyright 2012,2013 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H
#include <ch.h>
#include "drivers/qwiic/qwiic.h"
#include "action_layer.h"

// Define layer names
#define _NORMAL 0
#define _FNONE 1
#define _DVORAK 2
#define _COLEMAK 3

// Highly Modified by Xydane
const uint16_t keymaps[][MATRIX_ROWS][MATRIX_COLS] =
    {[_NORMAL] = LAYOUT_ortho(/*QWERTY*/
                KC_ESC,  KC_1,    KC_2,    KC_3,       KC_4,   KC_5,   KC_MINS, KC_GRV,  KC_EQL,  KC_6,   KC_7,   KC_8,    KC_9, KC_0, KC_BSPC,
                KC_TAB,  KC_Q,    KC_W,    KC_E,       KC_R,   KC_T,   KC_LBRC, KC_BSLS, KC_RBRC, KC_Y,   KC_U,   KC_I,    KC_O, KC_P, KC_QUOT,
                KC_CAPS, KC_A,    KC_S,    KC_D,       KC_F,   KC_G,   KC_HOME, KC_DEL,  KC_PGUP, KC_H,   KC_J,   KC_K,    KC_L, KC_SCLN, KC_ENT,
                KC_LSFT, KC_Z,    KC_X,    KC_C,       KC_V,   KC_B,   KC_END,  KC_UP,   KC_PGDN, KC_N,   KC_M,   KC_COMM, KC_DOT, KC_SLSH, KC_RSFT,
                KC_LCTL, KC_LGUI, KC_LALT, MO(_FNONE), KC_SPC, KC_SPC, KC_LEFT, KC_DOWN, KC_RGHT, KC_SPC, KC_SPC, TG(_DVORAK), KC_RALT, KC_APP, KC_RCTL),
    [_FNONE]  = LAYOUT_ortho(/* FUNCTION */
                KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_NLCK, KC_SLSH, KC_ASTR, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12,
                KC_MSEL, KC_CALC, KC_MYCM, KC_MAIL, RGB_HUD, RGB_HUI, KC_P7, KC_P8, KC_P9, KC_MINS, RESET, KC_ACL2, KC_PSCR, KC_SLCK, KC_PAUS,
                KC_MPRV, KC_MPLY, KC_MNXT, KC_MSTP, RGB_SAD, RGB_SAI, KC_P4, KC_P5, KC_P6, KC_PLUS, DEBUG, KC_ACL1, KC_BTN1, KC_BTN2, KC_BTN3,
                KC_VOLD, KC_MUTE, KC_VOLU, KC_APP, RGB_VAD, RGB_VAI, KC_P1, KC_P2, KC_P3, KC_PENT, _______, KC_ACL0, KC_BTN4, KC_WH_U, KC_BTN5,
                KC_BRIU, KC_BRID, RGB_TOG, MO(_FNONE), RGB_RMOD, RGB_MOD, KC_P0, _______, KC_PDOT, KC_PENT, KC_PENT, MO(_FNONE), KC_WH_L, KC_WH_D, KC_WH_R),
    [_DVORAK] = LAYOUT_ortho(/*DVORAK*/
                KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_MINS, KC_GRV, KC_EQL, KC_6, KC_7, KC_8, KC_9, KC_0, KC_BSPC,
                KC_TAB, KC_QUOT, KC_COMM, KC_DOT, KC_P, KC_Y, KC_LBRC, KC_BSLS, KC_RBRC, KC_F, KC_G, KC_C, KC_R, KC_L, KC_ENT,
                KC_CAPS, KC_A, KC_O, KC_E, KC_U, KC_I, KC_HOME, KC_DEL, KC_PGUP, KC_D, KC_H, KC_T, KC_N, KC_S, KC_ENT,
                KC_LSFT, KC_SCLN, KC_Q, KC_J, KC_K, KC_X, KC_END, KC_UP, KC_PGDN, KC_B, KC_M, KC_W, KC_V, KC_Z, KC_RSFT,
                KC_LCTL, KC_LGUI, KC_LALT, MO(_FNONE), KC_SPC, KC_SPC, KC_LEFT, KC_DOWN, KC_RGHT, KC_SPC, KC_SPC, TG(_COLEMAK), KC_RALT, KC_APP, KC_RCTL),
    [_COLEMAK] = LAYOUT_ortho(/*colemak*/
                KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_MINS, KC_GRV, KC_EQL, KC_6, KC_7, KC_8, KC_9, KC_0, KC_BSPC,
                KC_TAB, KC_Q, KC_W, KC_F, KC_P, KC_G, KC_LBRC, KC_BSLS, KC_RBRC, KC_J, KC_L, KC_U, KC_Y, KC_SCLN, KC_QUOT,
                KC_CAPS, KC_A, KC_R, KC_S, KC_T, KC_D, KC_HOME, KC_DEL, KC_PGUP, KC_H, KC_N, KC_E, KC_I, KC_O, KC_ENT,
                KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_END, KC_UP, KC_PGDN, KC_K, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT,
                KC_LCTL, KC_LGUI, KC_LALT, MO(_FNONE), KC_SPC, KC_SPC, KC_LEFT, KC_DOWN, KC_RGHT, KC_SPC, KC_SPC, TO(_NORMAL), KC_RALT, KC_RGUI, KC_RCTL)

};


/* Layer based ilumination, just binary */
layer_state_t layer_state_set_user(layer_state_t state) {
    switch (get_highest_layer(state)) {
        case _FNONE:
            palSetPad(GPIOA, 0);    // OFF Color A
            palClearPad(GPIOA, 1);  // ON Color B
            break;
        default:                    //  for any other layers, or the default layer
            palClearPad(GPIOA, 0);  // ON Color A
            palSetPad(GPIOA, 1);    // OFF Color B
            break;
    }
    return state;
}

uint16_t counterst = 0;
static bool queue_for_send = false;

#ifdef QWIIC_MICRO_OLED_ENABLE

/* screen off after this many milliseconds */
#include "timer.h"
#define ScreenOffInterval 60000 /* milliseconds */
static uint16_t last_flush;

volatile uint8_t led_numlock = false;
volatile uint8_t  led_capslock = false;
volatile uint8_t  led_scrolllock = false;

static uint8_t layer;
static uint8_t encoder_value = 32;

__attribute__ ((weak))
void draw_ui(void) {
  clear_buffer();
  last_flush = timer_read();
  send_command(DISPLAYON);

/* Layer indicator is 41 x 10 pixels */
#define LAYER_INDICATOR_X 5
#define LAYER_INDICATOR_Y 0

  draw_string(LAYER_INDICATOR_X + 1, LAYER_INDICATOR_Y + 2, "LAYER", PIXEL_ON, NORM, 0);
  draw_rect_filled_soft(LAYER_INDICATOR_X + 32, LAYER_INDICATOR_Y + 1, 9, 9, PIXEL_ON, NORM);
  draw_char(LAYER_INDICATOR_X + 34, LAYER_INDICATOR_Y + 2, layer + 0x30, PIXEL_ON, XOR, 0);

/* Matrix display is 19 x 9 pixels */
#define MATRIX_DISPLAY_X 5
#define MATRIX_DISPLAY_Y 18

  for (uint8_t x = 0; x < MATRIX_ROWS; x++) {
    for (uint8_t y = 0; y < MATRIX_COLS; y++) {
      draw_pixel(MATRIX_DISPLAY_X + y + 2, MATRIX_DISPLAY_Y + x + 2,(matrix_get_row(x) & (1 << y)) > 0, NORM);
    }
  }
  draw_rect_soft(MATRIX_DISPLAY_X, MATRIX_DISPLAY_Y, 19, 9, PIXEL_ON, NORM);
  /* hadron oled location on thumbnail */
  draw_rect_filled_soft(MATRIX_DISPLAY_X + 14, MATRIX_DISPLAY_Y + 2, 3, 1, PIXEL_ON, NORM);
/*
  draw_rect_soft(0, 13, 64, 6, PIXEL_ON, NORM);
  draw_line_vert(encoder_value, 13, 6, PIXEL_ON, NORM);

*/

/* Mod display is 41 x 16 pixels */
#define MOD_DISPLAY_X 30
#define MOD_DISPLAY_Y 18

  uint8_t mods = get_mods();
  if (mods & MOD_LSFT) {
    draw_rect_filled_soft(MOD_DISPLAY_X + 0, MOD_DISPLAY_Y, 5 + (1 * 6), 11, PIXEL_ON, NORM);
    draw_string(MOD_DISPLAY_X + 3, MOD_DISPLAY_Y + 2, "S", PIXEL_OFF, NORM, 0);
  } else {
    draw_string(MOD_DISPLAY_X + 3, MOD_DISPLAY_Y + 2, "S", PIXEL_ON, NORM, 0);
  }
  if (mods & MOD_LCTL) {
    draw_rect_filled_soft(MOD_DISPLAY_X + 10, MOD_DISPLAY_Y, 5 + (1 * 6), 11, PIXEL_ON, NORM);
    draw_string(MOD_DISPLAY_X + 13, MOD_DISPLAY_Y + 2, "C", PIXEL_OFF, NORM, 0);
  } else {
    draw_string(MOD_DISPLAY_X + 13, MOD_DISPLAY_Y + 2, "C", PIXEL_ON, NORM, 0);
  }
  if (mods & MOD_LALT) {
    draw_rect_filled_soft(MOD_DISPLAY_X + 20, MOD_DISPLAY_Y, 5 + (1 * 6), 11, PIXEL_ON, NORM);
    draw_string(MOD_DISPLAY_X + 23, MOD_DISPLAY_Y + 2, "A", PIXEL_OFF, NORM, 0);
  } else {
    draw_string(MOD_DISPLAY_X + 23, MOD_DISPLAY_Y + 2, "A", PIXEL_ON, NORM, 0);
  }
  if (mods & MOD_LGUI) {
    draw_rect_filled_soft(MOD_DISPLAY_X + 30, MOD_DISPLAY_Y, 5 + (1 * 6), 11, PIXEL_ON, NORM);
    draw_string(MOD_DISPLAY_X + 33, MOD_DISPLAY_Y + 2, "G", PIXEL_OFF, NORM, 0);
  } else {
    draw_string(MOD_DISPLAY_X + 33, MOD_DISPLAY_Y + 2, "G", PIXEL_ON, NORM, 0);
  }

/* Lock display is 23 x 32 */
#define LOCK_DISPLAY_X 100
#define LOCK_DISPLAY_Y 0

  if (led_numlock == true) {
    draw_rect_filled_soft(LOCK_DISPLAY_X, LOCK_DISPLAY_Y, 5 + (3 * 6), 9, PIXEL_ON, NORM);
    draw_string(LOCK_DISPLAY_X + 3, LOCK_DISPLAY_Y + 1, "NUM", PIXEL_OFF, NORM, 0);
  } else if (led_numlock == false) {
    draw_string(LOCK_DISPLAY_X + 3, LOCK_DISPLAY_Y + 1, "NUM", PIXEL_ON, NORM, 0);
  }
  if (led_capslock == true) {
    draw_rect_filled_soft(LOCK_DISPLAY_X + 0, LOCK_DISPLAY_Y + 11, 5 + (3 * 6), 9, PIXEL_ON, NORM);
    draw_string(LOCK_DISPLAY_X + 3, LOCK_DISPLAY_Y + 11 +1, "CAP", PIXEL_OFF, NORM, 0);
  } else if (led_capslock == false) {
    draw_string(LOCK_DISPLAY_X + 3, LOCK_DISPLAY_Y + 11 +1, "CAP", PIXEL_ON, NORM, 0);
  }

  if (led_scrolllock == true) {
    draw_rect_filled_soft(LOCK_DISPLAY_X + 0, LOCK_DISPLAY_Y + 22, 5 + (3 * 6), 9, PIXEL_ON, NORM);
    draw_string(LOCK_DISPLAY_X + 3, LOCK_DISPLAY_Y + 22 +1, "SCR", PIXEL_OFF, NORM, 0);
  } else if (led_scrolllock == false) {
    draw_string(LOCK_DISPLAY_X + 3, LOCK_DISPLAY_Y + 22 +1, "SCR", PIXEL_ON, NORM, 0);
  }
  send_buffer();
}

void read_host_led_state(void) {
  uint8_t leds = host_keyboard_leds();
  if (leds & (1 << USB_LED_NUM_LOCK))    {
    if (led_numlock == false){
    led_numlock = true;}
    } else {
    if (led_numlock == true){
    led_numlock = false;}
    }
  if (leds & (1 << USB_LED_CAPS_LOCK))   {
    if (led_capslock == false){
    led_capslock = true;}
    } else {
    if (led_capslock == true){
    led_capslock = false;}
    }
  if (leds & (1 << USB_LED_SCROLL_LOCK)) {
    if (led_scrolllock == false){
    led_scrolllock = true;}
    } else {
    if (led_scrolllock == true){
    led_scrolllock = false;}
    }
}

uint32_t layer_state_set_kb(uint32_t state) {
  state = layer_state_set_user(state);
  layer = biton32(state);
  queue_for_send = true;
  return state;
}

bool process_record_kb(uint16_t keycode, keyrecord_t *record) {
  queue_for_send = true;
  return process_record_user(keycode, record);
}

void encoder_update_kb(uint8_t index, bool clockwise) {
  encoder_value = (encoder_value + (clockwise ? 1 : -1)) % 64;
  queue_for_send = true;
}

#endif

#ifdef OLED_DRIVER_ENABLE

static void render_logo(void) {
    static const char PROGMEM qmk_logo[] = {0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0x8F, 0x90, 0x91, 0x92, 0x93, 0x94, 0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1, 0xB2, 0xB3, 0xB4, 0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xCB, 0xCC, 0xCD, 0xCE, 0xCF, 0xD0, 0xD1, 0xD2, 0xD3, 0xD4, 0x00};

    oled_write_P(qmk_logo, false);
}

void keyboard_post_init_user(void) {
    render_logo();
    wait_ms(2000);
    oled_init(OLED_ROTATION_180);
}

void oled_task_user(void) {
    // Host Keyboard Layer Status
    oled_write_P(PSTR("Layer: "), false);
    switch (get_highest_layer(layer_state)) {
        case _NORMAL:
            oled_write_P(PSTR("Default\n"), false);
            break;
        case _DVORAK:
            oled_write_P(PSTR("DVORAK\n"), false);
            break;
        case _COLEMAK:
            oled_write_P(PSTR("COLEMAK\n"), false);
            break;
        case _FNONE:
            oled_write_P(PSTR("FN\n"), false);
            break;
        default:
            // Or use the write_ln shortcut over adding '\n' to the end of your string
            oled_write_ln_P(PSTR("Undefined"), false);
    }

    // Host Keyboard LED Status
    uint8_t led_usb_state = host_keyboard_leds();
    oled_write_P(led_usb_state & (1 << USB_LED_NUM_LOCK) ?    PSTR("NUM    ") : PSTR("       "), false);
    oled_write_P(led_usb_state & (1 << USB_LED_CAPS_LOCK) ?   PSTR("CAP    ") : PSTR("       "), false);
    oled_write_P(led_usb_state & (1 << USB_LED_SCROLL_LOCK) ? PSTR("SCR    ") : PSTR("       "), false);
}

#endif

#ifdef ENCODER_ENABLE
/*uint16_t key_timer;
bool key_triggered = false;

static THD_WORKING_AREA(myThreadWorkingArea, 128);
void LED_ON(void){oled_write_ln_P(PSTR("Page Down"), false);}
void LED_OFF(void){oled_write_ln_P(PSTR("          "), false);}
static THD_FUNCTION(myThread, arg) {
  while (true) {
    LED_ON();
    chThdSleepMilliseconds(500);
    LED_OFF();
    chThdSleepMilliseconds(500);
  }
}*/
/*
#define ENC_RD	PINB	//encoder port read
#define ENC_CTL	DDRB	//encoder port control
#define ENC_WR	PORTB	//encoder port write
#define ENC_PIN_A PB4  //
#define ENC_PIN_B PB5  //
*/
/*
void matrix_init_user(void) {

  pinMode(ENC_PIN_A, PinDirectionInput);
  pinMode(ENC_PIN_B, PinDirectionInput);

  //turn on pullups
  ENC_WR |= (( 1<<ENC_PIN_A )|( 1<<ENC_PIN_B ));

}
int encA, encB;
int oldencA = 0;
int oldencB = 0;
int encCCW = 0;
int encCW = 0;
int counts = 0;
int pulsesPerStep = 1;

	static uint8_t prevState = 0;

uint8_t cwRotorState[4] = {0x10, 0x00, 0x11, 0x01};
uint8_t ccwRotorState[4] = {0x01, 0x11, 0x00, 0x10};

uint8_t State (void) {
	return(((PINF >> ENC_PIN_B) & 0x01) | (((PINF >> ENC_PIN_A) & 0x01) << 1));
}
*/
void encoder_update_user(uint8_t index, bool clockwise) {
    /*(void)chThdCreateStatic(myThreadWorkingArea, sizeof(myThreadWorkingArea),
                          NORMALPRIO, myThread, NULL);    key_timer = timer_read();*/
    //key_triggered = true;

    switch (get_highest_layer(layer_state)) {
        case _FNONE:
            if (clockwise) { tap_code(KC_VOLU); } else { tap_code(KC_VOLD);}
            break;
        default:
            if (clockwise) { tap_code(KC_WH_D); } else { tap_code(KC_WH_U);}
    }
}

/*void matrix_scan_user(void) {
  if ( key_triggered && ( timer_elapsed(key_timer) < 100 ) ) {
    // do the thing
  }
}*/


#endif

void matrix_init_kb(void) {
  queue_for_send = true;
	matrix_init_user();
}

void matrix_scan_kb(void) {
if (queue_for_send) {
#ifdef QWIIC_MICRO_OLED_ENABLE
   read_host_led_state();
   draw_ui();
#endif
   queue_for_send = false;
  }
#ifdef QWIIC_MICRO_OLED_ENABLE
  if (timer_elapsed(last_flush) > ScreenOffInterval) {
  send_command(DISPLAYOFF);      /* 0xAE */
  }
#endif
  if (counterst == 0) {
    //testPatternFB(o_fb);
  }
  counterst = (counterst + 1) % 1024;
  //rgblight_task();
	matrix_scan_user();
}
